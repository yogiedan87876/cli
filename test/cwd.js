const test = require('blue-tape')
const { prepare } = require('./helpers')
const { runSync } = require('./helpers/runner')
const { resolve, sep } = require('path')

test('run from any directory', async (t) => {
  await prepare('logged_in.json')
  const root = resolve(sep)
  const done = runSync('whoami', { cwd: root })
  t.is(done.stdout, 'johnw@example.net\n')
})
