# Usage

Run `commonshost` if you globally installed. Run `npx commonshost` if you locally installed.

## Create an account

```
$ commonshost signup

? Email address: someone@example.net
? Username: someone
? Password: ***************
 ✔ Registering account
 ✔ Creating new authentication token
 ✔ Saving credentials
```

## Use an existing account

```
$ commonshost login

? Username or email address: someone
? Password: [hidden]
 ✔ Authenticating
 ✔ Saving token
```

## Deploy a static web site

```
$ commonshost deploy --domain example.net --root ./ --confirm

To cancel, press Ctrl+C.

Deploying:

   Directory:  ~/blog/public
   File count: 14
   Total size: 24 kB
   URL:        https://example.net
   Options:    ~/blog/commonshost.json

 ✔ Uploading

Deployment successful!
```
